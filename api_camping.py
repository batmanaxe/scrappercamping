#api_camping.py

from flask import Flask, jsonify,request
from flask_restful import Api, Resource
from camping_car import camping_get_url_location

app = Flask(__name__)
api = Api(app)

def checkedPostedData(postedData,functionName):
	if "lat" not in postedData or "lon" not in postedData or "nbr_returned" not in postedData:
		return 301
	else:
		return 200

class Camping_location(Resource):
	def get(self):
		#step one, get posted data

		postedData = request.get_json()

		status_code = checkedPostedData(postedData,"Camping_location")

		if (status_code!=200):
			retJson = {
				"Message":" An error happened",
				"Status Code" : status_code
			}
			return jsonify(retJson)




		lat = postedData['lat']
		lon = postedData['lon']
		nbr_returned = postedData['nbr_returned']


		ret = camping_get_url_location(lat,lon,nbr_returned)
		retMap = {
			'Message': ret,
			'Status Code' : 200
		}

		return jsonify(retMap)

@app.route('/')
def hello():
	return"hello"


api.add_resource(Camping_location, "/Camping_location")





	
	

if __name__=="__main__":
    app.run(debug=True) 
