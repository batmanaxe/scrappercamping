import requests
from bs4 import BeautifulSoup as bs
import json

url="https://park4night.com/fr/lieu/133628/camping-avec-camping-cars-autoris%C3%A9s/l%C3%A8ge-cap-ferret-le-grand-crohot-oc%C3%A9an/france/gironde"
url_2 = "https://park4night.com/fr/lieu/76396/camping-avec-camping-cars-autoris%C3%A9s/carcans-35-route-de-lacanau/france/gironde"

def camping_info(url):

    response =requests.get(url)
    content=response.content
    parser = bs(content,'html.parser')
    nom = parser.find_all('div',class_='header_4')
    dict_camping={}
    if len(nom)==0:
        dict_camping={}
    elif len(nom)==1:
        nom_camping = nom[0].text
    else:
        nom_camping = nom[1].text
        
    textus = nom_camping.split()
    type_camping = textus[0]

    photos = parser.find_all("a",rel="prettyPhoto[gallery2]")
    link_image=[]
    for photo in photos:
        link = photo.get("href")
        if link not in link_image:
            link_image.append(link)

    gps = parser.find_all("div",id="window_footer_navigation_GPS")
    Latitude = gps[0].find(itemprop = "latitude").text
    Longitude = gps[0].find(itemprop = "longitude").text

    streetAddress = gps[0].find(itemprop = "streetAddress").text
    addressLocality = gps[0].find(itemprop = "addressLocality").text
    addressCountry = gps[0].find(itemprop = "addressCountry").text
    address = gps[0].find(itemprop = "address").text
        
    list_digit = address.split('\t\t\t')
    new_list = [ele.replace('\n','') for ele in list_digit]
    new_list2 = [ele.replace(' ','') for ele in new_list]
        
        
    if len(list_digit)>=2:
        zip_code = new_list2[1]
    else:
        zip_code=[]

    services = parser.find_all("div",id = "window_core_right_services")
    services1 = services[0].find_all("td")
    list_services1 = []
    for service in services1:
        if service.find_next_sibling("td")!=None:
            list_services1.append((service.text,service.find_next_sibling("td").text))

    services2 = parser.find_all("div",class_="grid_1")
    list_services2=[]
    for service in services2:
        img = service.find('img')


        service_name = img.get('title')
        if service_name!=None:
            list_services2.append(service_name)


    commentaires = parser.find_all("div",class_="window_footer_publicite_container_right")
    list_commentaires=[]
    for commentaire in commentaires:
        commentaire_text = commentaire.text
        commentaire_text = commentaire_text.replace('\n','')
        commentaire_text = commentaire_text.replace('\t','')
        list_commentaires.append(commentaire_text)

    nombre_com = int(len(list_commentaires))
    rating_commentaires = parser.find_all("div",class_="window_footer_publicite_container_left")
    list_rat_com = []
    s2 = "width"
    for rat in rating_commentaires:
        my_string = rat.find_all('div',class_="rating_fg")[0]['style']

        note = my_string[my_string.index(s2) + len(s2)+1:my_string.index(s2) + len(s2)+4]
        note = note.replace('p','')
        note = note.replace('x','')
        list_rat_com.append(note)

    joined_list = [(float(m),str(n)) for m,n in zip(list_rat_com,list_commentaires)]

    total_ratings = parser.find_all("div",class_="grid_11")
    my_string = total_ratings[1].find_all("div",class_="rating_fg")[0]['style']
    s2 = "width"
    note_total = my_string[my_string.index(s2) + len(s2)+1:my_string.index(s2) + len(s2)+4]
    note_total = note_total.replace('p','')
    note_total = note_total.replace('x','')
    note_total = float(note_total)

    
    dict_camping['type'] = type_camping
    dict_camping['Nom']=nom_camping
    dict_camping['streetAddress'] = streetAddress
    dict_camping['localité']=addressLocality
    dict_camping['pays']=addressCountry
    dict_camping['zip_code'] = zip_code
    dict_camping["Latitude"] = Latitude
    dict_camping["Longitude"] = Longitude
    dict_camping['Modalités des services'] = list_services1
    dict_camping['Type de services'] = list_services2
    dict_camping['Nombre de commentaires'] = nombre_com
    dict_camping['Notes et commentaires'] = joined_list
    dict_camping['Note moyenne'] = note_total
    dict_camping['photos'] = link_image
    
        
    
    return response.status_code,dict_camping


def camping_get_url_location(latitude=0,longitude=0,nbr_returned=1):
    if nbr_returned<=1:
        nbr_returned=1
    
    url_api ="https://park4night.com/services/V3/getLieuxAroundMeLite.php?"
    parameters = {'latitude':latitude,'longitude':longitude}
    response =requests.get(url_api,params = parameters)
    response.status_code
    content = response.content
    
    json1_data = json.loads(content)
    del json1_data['lieux'][nbr_returned-1:-1]
    for ele in json1_data['lieux']:
        ele['url']= "https://park4night.com/fr/lieu/"+str(ele['id'])+"/rand"
        ele['spec']=camping_info(ele['url'])
    
    

    
    return json1_data